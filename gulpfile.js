// Gulp module
var gulp = require('gulp');
var rename = require('gulp-rename');

// Sass module
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');

gulp.task('sass', function() {
    return gulp.src('./sass/estilo.scss')
      .pipe(rename('main.min.css'))
      .pipe(sass().on('error', sass.logError))
      .pipe(minifyCss({compatibility: 'ie8'}))
      .pipe(gulp.dest('./css'));
});

// Svg store module
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');

gulp.task('svgstore', function() {

    var config = {
        inlineSvg: true, // Remueve etiqueta <xml> y <!DOCTYPE>
    };

    return gulp
      .src('./svg/*.svg')
      .pipe(rename({prefix: 'icon-'}))
      .pipe(svgmin())
      .pipe(svgstore(config))
      .pipe(rename('sprite.svg'))
      .pipe(gulp.dest('./img'))
      .pipe(connect.reload());
});

var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

gulp.task('images', function() {
    return gulp.src(['./img/books/*', './img/foto.png'])
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
        }))
        .pipe(gulp.dest('./img/optimised/'));
});

// Connect livereload module
var connect = require('gulp-connect');
var open = require('gulp-open');

gulp.task('reload', function() {
    return gulp.src('./index.html')
        .pipe(connect.reload());
});

gulp.task('connect', function() {
    connect.server({
        livereload: true,
    });
    return gulp.src('./index.html')
    .pipe(open({uri: 'http://localhost:8080'}));
});

gulp.task('watch', function() {
    return gulp.watch('./sass/*.scss', ['sass']);
});